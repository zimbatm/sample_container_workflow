---
stages:
  - lint
  - test
  - build
  - contain


lint:app:
  stage: lint
  image: node:alpine
  cache:
    key: "app_node-depts"
    paths:
      - app/node_modules
  before_script:
    - cd app
    - node --version
    - npm --version
    - npm install
  script:
    - npm run lint

lint:server:fmt:
  stage: lint
  image: rustlang/rust:nightly-slim
  before_script:
    - cd server
    - rustc --version
    - cargo --version
    - rustup component add rustfmt-preview
  script:
    - cargo fmt --all -- --check

# lint:server:clippy:
#   stage: lint
#   image: rustlang/rust:nightly-slim
#   # cache:
#   #   key: "server_node-depts"
#   #   paths:
#   #     - server/node_modules
#   #     - /usr/local/cargo/registry/cache
#   before_script:
#     - cd server
#     - rustc --version
#     - cargo --version
#     - rustup component add clippy-preview
#   script:
#     - touch ./src && cargo clippy -- -D warnings

lint:yaml:
  stage: lint
  image: sdesbure/yamllint
  before_script:
    - yamllint --version
  script:
    - LIST_OF_YAML=$(find -path '.git' -prune -o -print | egrep "\.ya?ml$")
    - yamllint --strict --config-file .yamllint.yaml $LIST_OF_YAML

lint:markdown:
  stage: lint
  image:
    name: 06kellyjac/markdownlint-cli
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  before_script:
    - markdownlint --version
  script:
    - LIST_OF_MARKDOWN=$(find -path '.git' -prune -o -print | egrep "\.md$" | tr '\n' ' ')
    - markdownlint $LIST_OF_MARKDOWN

lint:docker:
  stage: lint
  image:
    name: hadolint/hadolint:latest-debian
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  before_script:
    - hadolint --version
  script:
    - LIST_OF_DOCKERFILES=$(find -path '.git' -prune -o -print | egrep ".*Dockerfile")
    - hadolint $LIST_OF_DOCKERFILES


test:app:
  stage: test
  image: node:alpine
  cache:
    key: "app_node-depts"
    paths:
      - app/node_modules
  before_script:
    - cd app
    - node --version
    - npm --version
    - npm install
  script:
    - npm run test:ci
  coverage: /All files\s*\|\s*([\d\.]+)/

test:server:
  stage: test
  image: rustlang/rust:nightly-slim
  cache:
    key: "server_test"
    paths:
      - /usr/local/cargo/registry/cache
  before_script:
    - cd server
    - rustc --version
    - cargo --version
  script:
    - cargo test


build:app:
  stage: build
  image: node:alpine
  artifacts:
    paths: [ app/build ]
  cache:
    key: app_node-depts
    paths:
      - app/node_modules
  before_script:
    - cd app
    - node --version
    - npm --version
    - npm install
  script:
    - npm run build

build:server:
  stage: build
  image: rustlang/rust:nightly-slim
  artifacts:
    paths: [ server/target/x86_64-unknown-linux-musl/release/server ]
  cache:
    key: "server_build"
    paths:
      - /usr/local/cargo/registry/cache
  before_script:
    - cd server
    - rustc --version
    - cargo --version
  script:
    - apt-get update && apt-get install -y musl-tools
    - rustup target add x86_64-unknown-linux-musl
    - cargo build --target x86_64-unknown-linux-musl --release
    - strip target/x86_64-unknown-linux-musl/release/server


contain:app:
  stage: contain
  only:
    - master
  dependencies:
    - build:app
  image: docker
  services:
    - docker:dind
  before_script:
    - cd app
    - docker --version
  script:
    - IMAGE_NAME="${CI_REGISTRY_IMAGE}/app"
    - echo ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - docker build -t $IMAGE_NAME -t ${IMAGE_NAME}:${CI_COMMIT_SHA} .
    - docker push $IMAGE_NAME
    - docker push ${IMAGE_NAME}:${CI_COMMIT_SHA}

contain:server:
  stage: contain
  only:
    - master
  dependencies:
    - build:server
  image: docker
  services:
    - docker:dind
  before_script:
    - cd server
    - docker --version
  script:
    - IMAGE_NAME="${CI_REGISTRY_IMAGE}/server"
    - echo ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - docker build -t $IMAGE_NAME -t ${IMAGE_NAME}:${CI_COMMIT_SHA} .
    - docker push $IMAGE_NAME
    - docker push ${IMAGE_NAME}:${CI_COMMIT_SHA}
